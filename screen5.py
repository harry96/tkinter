#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("screen 4")
window.geometry("300x300")

init_frame = tk.Frame(window)
init_frame.pack()

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)
third_frame = tk.Frame(window)
fourth_frame = {}
fifth_frame = []

def function1():
    first_frame.pack()
    tk.Label(first_frame, text="page1").pack()
    
    button_next = tk.Button(first_frame, text="Next", command=function2)
    button_next.pack()

def function2():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="page2").pack()
    
    button_next = tk.Button(second_frame, text="Next", command=lambda: function3(second_frame))
    button_next.pack()


def function3(previous_frame):
    previous_frame.destroy()
    third_frame.pack()
    tk.Label(third_frame, text="page3").pack()

    button_next = tk.Button(third_frame, text="Next", command=lambda: function4(third_frame))
    button_next.pack()

def function4(previous_frame):
    previous_frame.destroy()
    fourth_frame["f"] = tk.Frame(window)
    fourth_frame["f"].pack()
    mon_label = tk.Label(fourth_frame["f"], text="page4").pack()
    fourth_frame["tresor"] = "one piece doesn't exist"
    print(fourth_frame["tresor"]) 

    button_fourth = tk.Button(fourth_frame["f"], text="Next", command=lambda : function5(fourth_frame["f"] ,fourth_frame["tresor"])).pack()

def function5(*args):
    args[0].destroy()
    fifth_frame.insert(0, tk.Frame(window))
    f = fifth_frame[0]
    f.pack()  
    tk.Label(f, text="The " + args[1]).pack()

function1()
window.mainloop()
