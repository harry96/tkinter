#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("screen3")
window.geometry("300x300")

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)
third_frame = tk.Frame(window)


def function1():
    first_frame.pack()
    tk.Label(first_frame, text="page1").pack()

    button_next = tk.Button(first_frame, text="Next", command=function2)
    button_next.pack()

def function2():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="page2").pack()

    button_next = tk.Button(second_frame, text="Next", command=lambda: function3(second_frame))
    button_next.pack()

def function3(previous_frame):
    previous_frame.destroy()
    third_frame.pack()
    tk.Label(third_frame, text="page3").pack()

function1()
window.mainloop()

