#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("screen2")
window.geometry('300x300')


first_frame = tk.Frame(window)
second_frame = tk.Frame(window)

def function1():
    first_frame.pack()

    tk.Label(first_frame, text="page1").pack()
    
    button_next = tk.Button(first_frame, text='Next', command=function2)
    button_next.pack()


def function2():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="page2").pack()
 
function1()
window.mainloop()
